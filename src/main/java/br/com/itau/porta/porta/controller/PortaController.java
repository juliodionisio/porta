package br.com.itau.porta.porta.controller;

import br.com.itau.porta.porta.models.Porta;
import br.com.itau.porta.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta create(@RequestBody Porta porta) {

        porta = portaService.create(porta);

        return porta;
    }

    @GetMapping("/{id}")
    public Porta getById(@PathVariable Long id) {
        return portaService.getById(id);
    }

}
